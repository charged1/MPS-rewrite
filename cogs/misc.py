import discord, os, json
from discord.ext import commands
from discord.ext.commands import bot
from discord.commands import slash_command

if os.path.exists(os.getcwd() + "/config.json"):
    with open("./config.json")as f:
        configData = json.load(f)

else:
    pass

guildID = int(configData["GuildID"])

def setup(bot):
    bot.add_cog(slashcmds(bot))

class slashcmds(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(guild_ids=[guildID], description="Makes the bot say something")
    async def say(self, ctx, *, message):
        await ctx.respond(message)
    
    #slash command to reply with ping in ms
    @commands.slash_command(guild_ids=[guildID], description="Replies with ping in ms.")
    async def ping(self, ctx):
        await ctx.respond(f"Pong! {round(self.bot.latency * 1000)}ms")
